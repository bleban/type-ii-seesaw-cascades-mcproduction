# Instructions for the MC Generation / Derivation / Validation of several LRSM signal processes

Below, you can find the instructions on how to start with the MC generation and go through the derivation process to finally obtain the validation plots of several LRSM and ALP signal processes. Three UFO models [[TypeII_NLO_v_1_4_UFO](https://feynrules.irmp.ucl.ac.be/wiki/TypeIISeesaw#no1), [mlrsm-nu-loop](https://feynrules.irmp.ucl.ac.be/wiki/LRSM_NLO), [ALP_linear_UFO_WIDTH](http://feynrules.irmp.ucl.ac.be/wiki/ALPsEFT)] are contained in the repository. Note that the whole process can be done in just four steps:

```shell
./prepareJOs.sh # prepare the joboptions
./generate.sh   # generate events
./derivation.sh # do the derivation
run           # do the analysis and plots
```

However, scripts require a different environmental setup, with the detailed instructions given below. All of these scripts are equipped with the `-h`/`--help` flag for some help and have some useful flags.

## MC Sample Generation

1. Prepare the environment:

    ```shell
    setupATLAS
    asetup AthGeneration,main,latest
    ```

    NOTE: If you are running the code on `f9pc`, be sure to unset the `PYTHIA` environmental variables __before__ `asetup` command, e.g. by `unset $(env | grep PYTHIA8 | grep -E -o '^[^=]+')`.

2. Prepare the job options (JOs) with the `prepareJOs.sh` script. It will create a folder `joboptions` with `<TYPE>/<DSID>` subfolders. Your first JO DSID folder (usually 100000) will contain the control file `MadGraphControl_<MODEL>.py` which will be symlinked to all the other JO DSID folders. Additionally, a one-line `.py` JO will be created in each JO DSID folder, named accordingly to your masspoint selection and the control file will make sure the correct BSM masses are deduced from the `physicsShort` part of the JO filenames. The script will also prepare a configuration file called `config.sh`, which will be needed for input/output file paths in the next steps - of course, it can be manually modified.

3. Simulate the signal events by running the `generate.sh` script. The script will produce the `EVNT.<DSID>.<MASSPOINT>.root` files inside the `EVNTs/<TYPE>/<DSID>` directory.

## Producing the TRUTH derivations

1. Prepare a new, clean environment:

    ```shell
    setupATLAS
    asetup Athena,main,latest
    ```

2. Produce the `TRUTH1` and `TRUTH3` derivations by running the `derivation.sh` script. The script will produce the `DAOD_TRUTHs` folder with the `<TYPE>/<DSID>` subfolders containing the log files and final `DAOD_TRUTH<1/3>.<DSID>.<MASSPOINT>.pool.root` files for each signal mass sample.

## Producing the Validation histograms (following the [MCTutorial](https://atlassoftwaredocs.web.cern.ch/analysis-software/AnalysisSWTutorial/mc_validation/)) and plotting

1. Navigate to the `validation` folder and prepare the clean environment:

    ```shell
    setupATLAS
    source bin/setup.sh
    source bin/compile.sh
    ```

    If there are __NO__ errors, use the following command:

    ```shell
    source build/x86_64*/setup.sh
    ```

    From now on, you will be able to use `setup` (setup the environment), `compile` (recompile the code after your changes), and `run` (run analysis and plot histograms) commands from anywhere to manage the validation package and do the analysis.

2. You are now ready to run the analysis on all `TRUTH1`, `TRUTH3`, and `PHYS` (which contain `TRUTH3` information) derivations (maybe also others work but are not tested). The output of the analysis is a combination of relevant histograms in a form of a `.pdf` file. You run all this with just one command:

    ```shell
    run
    ```

    The `run` script has several flags:
    - `-l`/`--log-level` to choose the logging level,
    - `-p`/`--plot-only` to only use the plotting part of the code,
    - `-f`/`--format` to only run on the desired derivation format,
    - `-d`/`--dsid` to only run on the desired DSID range (must be in a `minDSID-maxDSID` format), and
    - `--detector` to consider the detector acceptance.

    The script will create a directory for each signal sample named `<TYPE>/DAOD_<FORMAT>_<DSID>_<MASSPOINT>` that contains several files and directories. The files, named `hist-<DSID>.root`, contain histograms showing several validation distributions. `data-ANALYSIS/<DSID>.root` contains a `TTree` that stores similar validation information. The `run` script also prepares a combination of all plots using the `plot` executable and stores them into a `validation_histograms_{type}_{format}.pdf` set of plots. The `plot` executable can be run standalone but be careful to first source the `config.sh` and provide the input path that is mandatory.
