#!/bin/bash

help() {
    echo "This script is used in a CI job to merge log.generate files from GRIDPACK and EVNT generation steps."
    echo -e "Usage: move_artifacts.sh <flags>\n"
    echo "Available flags:"
    echo -e "\t+ [ -h | --help       ] Print out this message and exit."
    echo -e "\t+ [ -p | --path       ] Path to the EVNTs folder. Default: None"
    echo -e "\t+ [ -r | --release    ] Type of release. Default: v23_6_22."
    echo -e "\t+ [ -o | --output-dir ] Output directory. Default: merged_logs."
    exit 0
}

# Defaults
EVNT_PATH=""
RELEASE=v23_6_22
OUTPUT_DIR="merged_logs"
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"

# Prepare logging
# shellcheck source=../utils/logging.sh
source "${SCRIPT_DIR}/../utils/logging.sh"

# Command parsing
getopt --test >/dev/null
if [[ $? -ne 4 ]]; then
    log_message "merge_logs" "${RED}$(getopt --test) failed in this environment${ENDCOLOR}."
    exit 1
fi

opt_short=h,p:,r:,o:
opt_long=help,path:,release:,output-dir:
OPTS=$(getopt --options $opt_short --longoptions $opt_long -- "$@")

eval set -- "$OPTS"

while true; do
    case "$1" in
    -h | --help)
        help
        ;;
    -p | --path)
        printf "${BLUE}ARGPARSER${ENDCOLOR}: Will work in the EVNT_PATH=%s.\n" "$2"
        EVNT_PATH="$2"
        shift 2
        ;;
    -r | --release)
        printf "${BLUE}ARGPARSER${ENDCOLOR}: Working with %s release.\n" "$2"
        RELEASE="$2"
        shift 2
        ;;
    -o | --output-dir)
        printf "${BLUE}ARGPARSER${ENDCOLOR}: Will save the results in %s.\n" "$2"
        OUTPUT_DIR="$2"
        shift 2
        ;;
    --)
        shift
        break
        ;;
    *)
        printf "${RED}ARGPARSER${ENDCOLOR}: Unexpected option: %s.\n" "$1"
        exit 1
        ;;
    esac
done

if [[ -z "$EVNT_PATH" || ! -d "$EVNT_PATH" ]]; then
    log_message "merge_logs" "${RED}Path to EVNTs folder (-p or --path) is required. Exiting...${ENDCOLOR}"
    exit 1
fi

# Create output directory if it doesn't exist
if [[ ! -d "$OUTPUT_DIR" ]]; then
    log_message "merge_logs" "Creating output directory: $OUTPUT_DIR."
   if ! mkdir -p "$OUTPUT_DIR"; then
        log_message "merge_logs" "${RED}Failed to create output directory ${OUTPUT_DIR}. Exiting...${ENDCOLOR}"
        exit 1
    fi
fi

# Merging logs
for DSID_PATH in "${EVNT_PATH}/production/"*; do
    DSID="$(basename "$DSID_PATH")"

    log_message "merge_logs" "Merging logs for DSID: ${RED}${DSID}${ENDCOLOR} and release ${RED}${RELEASE}${ENDCOLOR}."

    # Check if both log files exist
    PROD_LOG="${EVNT_PATH}/production/${DSID}/log.generate"
    GEN_LOG="${EVNT_PATH}/generation/${DSID}/log.generate"

    # Concatenate logs into the output file
    if [[ -f "$PROD_LOG" && -f "$GEN_LOG" ]]; then
        cat "$PROD_LOG" "$GEN_LOG" > "${OUTPUT_DIR}/log.generate.${RELEASE}.${DSID}"
    else
        if [[ ! -f "$PROD_LOG" ]]; then
            log_message "merge_logs" "${RED}Production log missing for DSID ${DSID} and release ${RELEASE}${ENDCOLOR}."
        fi
        if [[ ! -f "$GEN_LOG" ]]; then
            log_message "merge_logs" "${RED}Generation log missing for DSID ${DSID} and release ${RELEASE}${ENDCOLOR}."
        fi
        log_message "merge_logs" "${RED}Exiting...${ENDCOLOR}"
        exit 1
    fi
done
