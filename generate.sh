#!/bin/bash

help() {
    echo "This script runs the MCGeneration from the joboptions using the AthGeneration's Gen_tf.py script."
    echo -e "Usage: generate.sh <flags>\n"
    echo "Available flags:"
    echo -e "\t+ [ -h | --help           ] Print out this message."
    echo -e "\t+ [ -j | --num-jobs       ] Number of jobs that you want to submit in parallel. Default: 4."
    echo -e "\t+ [ -i | --input-path     ] Input path where you stored joboptions. It is read from the sourced config.sh by default."
    echo -e "\t+ [ -e | --max-events     ] Generate max-events events. Default: 10000."
    echo -e "\t+ [    | --ecm-energy     ] Beam energy in GeV. Default: 13600."
    echo -e "\t+ [ -s | --random-seed    ] Random seed used for MC generation. Default: 123456."
    echo -e "\t+ [    | --official-model ] Whether to use the official ATLAS models from /cvmfs."
    echo -e "\t                            Used e.g. for producing validation log files before the official production. Default: false."
    exit 0
}

# Prepare logging
source "utils/logging.sh"

# Default values
SHIFT="\t\t\t"
NUM_JOBS=4
MAXEVENTS=10000
ECMENERGY=13600
RANDOMSEED=123456
OFFICIALMODEL=false
PROJECT_WORKING_DIR=$(pwd)

# Command parsing
getopt --test >/dev/null
if [[ $? -ne 4 ]]; then
    log_message "ARGPARSER" "${RED}$(getopt --test) failed in this environment.${ENDCOLOR}"
    exit 1
fi

opt_short=h,j:,i:,e:,s:
opt_long=help,num-jobs:,input-path:,max-events:,ecm-energy:,random-seed:,official-model
OPTS=$(getopt --options $opt_short --longoptions $opt_long -- "$@")

eval set -- "$OPTS"

while true; do
    case "$1" in
    -h | --help)
        help
        ;;
    -j | --num-jobs)
        log_message "ARGPARSER" "Submittig $2 jobs in parallel."
        NUM_JOBS="$2"
        shift 2
        ;;
    -i | --input-path)
        log_message "ARGPARSER" "Inputs will be read from: $2/joboptions."
        JO_PATH="$2/joboptions"
        shift 2
        ;;
    -e | --max-events)
        log_message "ARGPARSER" "Generating $2 events."
        MAXEVENTS="$2"
        shift 2
        ;;
    --ecm-energy)
        log_message "ARGPARSER" "Generating at $2 GeV beam energy."
        ECMENERGY="$2"
        shift 2
        ;;
    -s | --random-seed)
        log_message "ARGPARSER" "Using $2 as random seed."
        RANDOMSEED="$2"
        shift 2
        ;;
    --official-model)
        log_message "ARGPARSER" "Using the models from /cvmfs."
        OFFICIALMODEL=true
        shift
        ;;
    --)
        shift
        break
        ;;
    *)
        log_message "ARGPARSER" "Unexpected option: ${RED}${1}${ENDCOLOR}."
        ;;
    esac
done

# Only do NUM_JOBS in parallel
maxjobs() {
    while [[ $(jobs -r | wc -l) -ge "$NUM_JOBS" ]]; do
        sleep 10
    done
}

# Export PYTHONPATH to find the local UFO model files
if ! $OFFICIALMODEL; then
    export PYTHONPATH="$PROJECT_WORKING_DIR:$PYTHONPATH"
fi

# Check that the input path is set correctly.
# shellcheck source=config.sh
source config.sh
if [[ ! -v JO_PATH || ! -v EVNT_PATH ]]; then
    log_message "MCGeneration" "${RED}ERROR${ENDCOLOR}: "'"JO_PATH"'" or "'"EVNT_PATH"'" path variable is not set. Check your "'"source config.sh"'" first. It should be produced by running the prepareJOs.sh script. Exiting..."
    exit 2
fi

# Checks for the GRIDPACK mode and DELETION of folders
if [[ "$EVNT_PATH" == *"GRIDPACKS"* ]]; then
    while true; do
        log_message "MCGeneration" "Running in ${RED}GRIDPACK${ENDCOLOR} mode. Are you sure you want to proceed? [Y/N]"
        read -r YESNO
        case $YESNO in
        [Yy]*)
            break
            ;;
        [Nn]*)
            log_message "MCGeneration" "Exiting..."
            exit
            ;;
        *)
            log_message "MCGeneration" "Please answer Y or N."
            ;;
        esac
    done

    GRIDPACKS=$(find "$(dirname "$EVNT_PATH")" -maxdepth 3 -type f -name "*.GRID.tar.gz" 2>/dev/null | sort -V)
    if [[ -n "$GRIDPACKS" ]]; then
        while true; do
            log_message "MCGeneration" "Found existing ${RED}GRIDPACKS${ENDCOLOR}. Do you want to run on this (Y), exit (N) or remove folder and start from scratch (D)? [Y/N/D]"
            read -r YESNO
            case $YESNO in
            [Yy]*)
                # Extract .tar files if not yet extracted
                echo "$GRIDPACKS" | while IFS= read -r TAR_FILE; do
                    if [[ -d "$(dirname "$TAR_FILE")/madevent" ]]; then
                        continue
                    fi
                    log_message "MCGeneration" "Extracting $TAR_FILE."
                    tar -xzf "$TAR_FILE" -C "$(dirname "$TAR_FILE")"
                done
                break
                ;;
            [Nn]*)
                log_message "MCGeneration" "Exiting..."
                exit
                ;;
            [Dd]*)
                rm -r "${EVNT_PATH:?}"
                log_message "MCGeneration" "Finished deleting files..."
                break
                ;;
            *)
                log_message "MCGeneration" "Please answer Y, N or D."
                ;;
            esac
        done
    else
        log_message "MCGeneration" "${RED}GRIDPACKS${ENDCOLOR} do not exist yet. Will produce them now..."
    fi

    # Specify the number of cores to run on
    NUM_JOs=$(find "$JO_PATH"/*/mc.*.py -type f | wc -l)
    if [[ "$NUM_JOs" -lt "$NUM_JOBS" ]]; then
        NUM_JOBS="$NUM_JOs"
    fi
    NUM_CORES=$(($(nproc --all) / NUM_JOBS))
    export ATHENA_PROC_NUMBER=$NUM_CORES
    log_message "MCGeneration" "Running on $NUM_CORES cores."

else
    if [[ -d "$EVNT_PATH" ]]; then
        while true; do
            log_message "MCGeneration" "The OUTPUT destination ${BLUE}EVNT_PATH=${EVNT_PATH}${ENDCOLOR} already exists. If you proceed, it will be overwritten. Are you sure you want to proceed? [Y/N]"
            read -r YESNO
            case $YESNO in
            [Yy]*)
                find "$EVNT_PATH" -mindepth 1 -maxdepth 1 -type d ! -name "GRIDPACKS" -exec rm -r {} +
                log_message "MCGeneration" "Finished deleting files..."
                break
                ;;
            [Nn]*)
                log_message "MCGeneration" "Exiting..."
                exit
                ;;
            *)
                log_message "MCGeneration" "Please answer Y or N."
                ;;
            esac
        done
    fi
fi

# Loop through the files and run the generation
for JO in "$JO_PATH"/*/mc.*.py; do
    maxjobs

    # Extract info from the file name
    JO_DIRNAME=$(dirname "$JO")
    DSID=$(basename "$(dirname "$JO")")

    mkdir -p "$EVNT_PATH/$DSID"

    # Logging
    if [[ "$TYPE" == "CASCADES" || "$TYPE" == "ALPS" ]]; then
        MASS=$(basename "$JO" | cut -f4 -d_ | sed -n 's/[^0-9]\+\([0-9]\+\).*/\1/p')
        log_message "MCGeneration" "Running for mass ${RED}${MASS}${ENDCOLOR} GeV with DSID ${RED}${DSID}${ENDCOLOR}."
        MASS="m$MASS"
    elif [[ "$TYPE" == "2HNS" || "$TYPE" == "4HNS" ]]; then
        MN=$(basename "$JO" | cut -f4 -d_ | sed -n 's/[^0-9]\+\([0-9]\+\).*/\1/p')
        MDD=$(basename "$JO" | cut -f5 -d_ | sed -n 's/[^0-9]\+\([0-9]\+\).*/\1/p')
        log_message "MCGeneration" "Running for masses ${RED}${MN} : ${MDD}${ENDCOLOR} GeV (mN:mDD) with DSID ${RED}${DSID}${ENDCOLOR}."
        MASS="N${MN}_DD${MDD}"
    fi

    if [[ $ECMENERGY == 13600 && "$TYPE" == "ALPS" ]]; then
        ECMENERGY=5020
    fi

    # Go into the OUTPUT directory to save the logs there
    pushd "$EVNT_PATH/$DSID" >/dev/null || exit

    log_message "MCGeneration" "Executing:"
    echo ""
    echo -e "${SHIFT}Gen_tf.py --ecmEnergy=$ECMENERGY \\"
    echo -e "${SHIFT}          --maxEvents=$MAXEVENTS \\"
    echo -e "${SHIFT}          --randomSeed=$RANDOMSEED \\"
    echo -e "${SHIFT}          --outputEVNTFile=EVNT.$DSID.$MASS.root \\"
    echo -e "${SHIFT}          --jobConfig=$JO_DIRNAME"
    echo ""

    Gen_tf.py --ecmEnergy="$ECMENERGY" \
        --maxEvents="$MAXEVENTS" \
        --randomSeed="$RANDOMSEED" \
        --outputEVNTFile="EVNT.$DSID.$MASS.root" \
        --jobConfig="$JO_DIRNAME" \
        &
    popd >/dev/null || exit

done
wait

# Cleanup gridpacks for grid if found only on gridpack production step (not generation)
FIND_MADEVENT=$(find "${EVNT_PATH}" -type d -name "madevent")
if [ -z "$FIND_MADEVENT" ]; then
    FIND_GRIDPACKS=$(find "${EVNT_PATH}" -type f -name "*.GRID.tar.gz" | sort -V)
    if [ -n "$FIND_GRIDPACKS" ]; then
        log_message "MCGeneration" "Gridpacks ${GREEN}exist${ENDCOLOR} and are located here:"
        echo "$FIND_GRIDPACKS" | xargs ls -lh

        echo "$FIND_GRIDPACKS" | while read -r TAR_FILE; do
            log_message "MCGeneration" "Working on $(basename "$TAR_FILE") file..."
            tar -xzf "$TAR_FILE" -C "$(dirname "$TAR_FILE")"
            pushd "$(dirname "$TAR_FILE")/madevent" >/dev/null || exit
            # shellcheck source=/dev/null
            source bin/clean4grid
            popd >/dev/null || exit
            tar -czf "$TAR_FILE" -C "$(dirname "$TAR_FILE")" madevent
        done
    fi
fi

# Cleanup extracted gridpacks if found
FIND_MADEVENT=$(find "${EVNT_PATH}" -type d -name "madevent")
if [ -n "$FIND_MADEVENT" ]; then
    echo "$FIND_MADEVENT" | xargs rm -r
    log_message "MCGeneration" "Deleting extracted madevent folders in gridpack..."
fi

echo
log_message "MCGeneration" "Script execution completed successfully."
