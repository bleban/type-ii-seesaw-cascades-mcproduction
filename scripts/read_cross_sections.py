#!/usr/bin/env python3

import os
import re
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


def read_and_plot(Cs: list, masses: list) -> int:

    FIRST_EVNT_PATH = os.environ.get('EVNT_PATH')
    assert FIRST_EVNT_PATH is not None, 'Environment variable EVNT_PATH is not defined!'

    EVNT_PATH = os.path.dirname(os.environ.get('EVNT_PATH'))
    DSIDS = sorted(os.listdir(FIRST_EVNT_PATH))

    xsec_pattern = r'[-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?'

    xsecs, widths, partial = np.zeros((3, len(Cs), len(Cs), len(DSIDS)))
    for i, cbtil in enumerate(Cs):
        for j, caphi in enumerate(Cs):
            curr_dir = os.path.join(EVNT_PATH, f'cbtil_{cbtil}_caphi_{caphi}')
            assert sorted(os.listdir(curr_dir)) == DSIDS, 'DSIDS do not match!'

            for k, dsid in enumerate(DSIDS):

                # Read cross-sections
                with open(os.path.join(curr_dir, dsid, 'log.generate')) as logfile:
                    for line in logfile:
                        if 'Cross-section :' in line:
                            assert 'pb' in line, 'Cross-section is not in pb!'
                            full_xsec = line.split('Cross-section :')[1]
                            match = re.search(xsec_pattern, full_xsec).group()
                            xsecs[i, j, k] = float(match)
                            break

                # Read Widths
                with open(os.path.join(curr_dir, dsid, 'events.lhe')) as lhefile:
                    for line in lhefile:
                        if 'DECAY  9000005' in line:
                            widths[i, j, k] = line.split('DECAY  9000005')[1].strip()
                        if '13  -13' in line:
                            partial[i, j, k] = line.split('#')[1].strip()
                            break

        with PdfPages('ALP_properties.pdf') as pdf:
            for m, mass_point in enumerate(masses):
                plt.figure(figsize=(18, 6))
                plt.suptitle(rf'Cross-sections [pb], Widths [GeV] and Partial Widths [GeV] for $m_{{ALP}}$ = {mass_point} GeV')
                for q, quantity in enumerate([xsecs, widths, partial]):
                    plt.subplot(1, 3, q + 1)

                    plt.imshow(quantity[:, :, m], norm='log')

                    # Print values on the grid
                    for i in range(len(Cs)):
                        for j in range(len(Cs)):
                            value = quantity[i, j, m]
                            threshold = np.quantile(quantity, 0.21)
                            plt.text(j - 0.25, i, f'{value:.3e}', c='w' if value <= threshold else 'k')

                    # Axes adjustments
                    plt.xlabel(r'$C_{a\Phi}$')
                    plt.ylabel(r'$C_{\tilde{B}}$')
                    plt.xticks(np.linspace(0, len(Cs) - 1, len(Cs)), Cs)
                    plt.yticks(np.linspace(0, len(Cs) - 1, len(Cs)), Cs)
                    plt.tight_layout()
                pdf.savefig()

    return 0


if __name__ == '__main__':

    Cs = ['10', '1', '1e-2', '1e-5']
    masses = [10, 20, 30, 50]
    read_and_plot(Cs, masses)
