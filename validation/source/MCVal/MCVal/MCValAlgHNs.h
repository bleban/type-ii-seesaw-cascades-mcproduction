#ifndef MCVal_MCValAlgHNs_H
#define MCVal_MCValAlgHNs_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <MCTruthClassifier/MCTruthClassifier.h>

#include <TH1.h>
#include <TH2.h>
#include <numeric>
#include <TTree.h>
#include <Math/VectorUtil.h>

#include "MCVal/MCValUtils.h"

typedef ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double>> LorentzVector;

class MCValAlgHNs : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MCValAlgHNs(const std::string &name, ISvcLocator *pSvcLocator);

  ~MCValAlgHNs() override;

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

  // Algorithm properties
  int m_pdgIdDD;
  int m_pdgIdN1;
  int m_pdgIdN2;
  int m_pdgIdN3;
  std::vector<int> m_mediators;
  bool m_detector;

  // Event properties to save
  unsigned int m_runNumber = 0;         ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number

  float m_met;

  std::vector<float> *m_dd_pt;
  std::vector<float> *m_dd_eta;
  std::vector<float> *m_dd_phi;
  std::vector<float> *m_dd_m;
  std::vector<int> *m_dd_pdgid;

  std::vector<float> *m_n1_pt;
  std::vector<float> *m_n1_eta;
  std::vector<float> *m_n1_phi;
  std::vector<float> *m_n1_m;
  std::vector<int> *m_n1_pdgid;

  std::vector<float> *m_n2_pt;
  std::vector<float> *m_n2_eta;
  std::vector<float> *m_n2_phi;
  std::vector<float> *m_n2_m;
  std::vector<int> *m_n2_pdgid;

  std::vector<float> *m_n3_pt;
  std::vector<float> *m_n3_eta;
  std::vector<float> *m_n3_phi;
  std::vector<float> *m_n3_m;
  std::vector<int> *m_n3_pdgid;

  std::vector<float> *m_lep_pt;
  std::vector<float> *m_lep_eta;
  std::vector<float> *m_lep_phi;
  std::vector<float> *m_lep_m;
  std::vector<int> *m_lep_pdgid;

  std::vector<float> *m_el_pt;
  std::vector<float> *m_el_eta;
  std::vector<float> *m_el_phi;
  std::vector<float> *m_el_m;

  std::vector<float> *m_mu_pt;
  std::vector<float> *m_mu_eta;
  std::vector<float> *m_mu_phi;
  std::vector<float> *m_mu_m;

  std::vector<float> *m_tau_pt;
  std::vector<float> *m_tau_eta;
  std::vector<float> *m_tau_phi;
  std::vector<float> *m_tau_m;

  std::vector<float> *m_q_pt;
  std::vector<float> *m_q_eta;
  std::vector<float> *m_q_phi;
  std::vector<float> *m_q_m;
  std::vector<int> *m_q_pdgid;

  std::vector<float> *m_jet_pt;
  std::vector<float> *m_jet_eta;
  std::vector<float> *m_jet_phi;
  std::vector<float> *m_jet_m;

private:
  // Configuration, and any other types of variables go here.
  // float m_cutValue;
  // TTree *m_myTree;
  // TH1 *m_myHist;
  int elFromN2 = 0;
  int elFromN3 = 0;
  int muFromN1 = 0;
  int muFromN3 = 0;
  int tauFromN1 = 0;
  int tauFromN2 = 0;
  unsigned int noDD = 0;
  std::string m_format;

  /// \brief the accessor for \ref m_classificationDecoration
  std::unique_ptr<const SG::AuxElement::ConstAccessor<unsigned int> > m_mctcAccessor{};
};

#endif
