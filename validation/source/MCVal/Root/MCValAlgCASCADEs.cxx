#include <AsgMessaging/MessageCheck.h>
#include <MCVal/MCValAlgCASCADEs.h>

MCValAlgCASCADEs::MCValAlgCASCADEs(const std::string &name,
                                   ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm(name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. This is also where you
  // declare all properties for your algorithm. Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("pdgIdD0", m_pdgIdD0 = 9000005, "Truth D0 PDGId");
  declareProperty("pdgIdDP", m_pdgIdDP = 38, "Truth DP PDGId");
  declareProperty("pdgIdDPP", m_pdgIdDPP = 61, "Truth DPP PDGId");
  declareProperty("pdgIdChi", m_pdgIdChi = 62, "Truth Chi PDGId");
  declareProperty("format", m_format = "TRUTH1", "Derivation format");
  declareProperty("detector", m_detector = false, "Switch on the detector acceptance");
}

StatusCode MCValAlgCASCADEs::initialize()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees. This method gets called before any input files are
  // connected.

  // Create output tree
  ANA_CHECK(book(TTree("analysis", "My analysis ntuple")));
  TTree *mytree = tree("analysis");

  // Declare branches
  mytree->Branch("RunNumber", &m_runNumber);
  mytree->Branch("EventNumber", &m_eventNumber);

  // Prepare branches for D0
  m_d0_pt = new std::vector<float>();
  m_d0_eta = new std::vector<float>();
  m_d0_phi = new std::vector<float>();
  m_d0_m = new std::vector<float>();
  m_d0_pdgid = new std::vector<int>();

  mytree->Branch("d0_pt", &m_d0_pt);
  mytree->Branch("d0_eta", &m_d0_eta);
  mytree->Branch("d0_phi", &m_d0_phi);
  mytree->Branch("d0_m", &m_d0_m);
  mytree->Branch("d0_pdgid", &m_d0_pdgid);

  // Prepare branches for DP
  m_dp_pt = new std::vector<float>();
  m_dp_eta = new std::vector<float>();
  m_dp_phi = new std::vector<float>();
  m_dp_m = new std::vector<float>();
  m_dp_pdgid = new std::vector<int>();

  mytree->Branch("dp_pt", &m_dp_pt);
  mytree->Branch("dp_eta", &m_dp_eta);
  mytree->Branch("dp_phi", &m_dp_phi);
  mytree->Branch("dp_m", &m_dp_m);
  mytree->Branch("dp_pdgid", &m_dp_pdgid);

  // Prepare branches for DPP
  m_dpp_pt = new std::vector<float>();
  m_dpp_eta = new std::vector<float>();
  m_dpp_phi = new std::vector<float>();
  m_dpp_m = new std::vector<float>();
  m_dpp_pdgid = new std::vector<int>();

  mytree->Branch("dpp_pt", &m_dpp_pt);
  mytree->Branch("dpp_eta", &m_dpp_eta);
  mytree->Branch("dpp_phi", &m_dpp_phi);
  mytree->Branch("dpp_m", &m_dpp_m);
  mytree->Branch("dpp_pdgid", &m_dpp_pdgid);

  // Prepare branches for chi
  m_chi_pt = new std::vector<float>();
  m_chi_eta = new std::vector<float>();
  m_chi_phi = new std::vector<float>();
  m_chi_m = new std::vector<float>();
  m_chi_pdgid = new std::vector<int>();

  mytree->Branch("chi_pt", &m_chi_pt);
  mytree->Branch("chi_eta", &m_chi_eta);
  mytree->Branch("chi_phi", &m_chi_phi);
  mytree->Branch("chi_m", &m_chi_m);
  mytree->Branch("chi_pdgid", &m_chi_pdgid);

  // Prepare branches for leptons
  m_lep_pt = new std::vector<float>();
  m_lep_eta = new std::vector<float>();
  m_lep_phi = new std::vector<float>();
  m_lep_m = new std::vector<float>();
  m_lep_pdgid = new std::vector<int>();

  mytree->Branch("lep_pt", &m_lep_pt);
  mytree->Branch("lep_eta", &m_lep_eta);
  mytree->Branch("lep_phi", &m_lep_phi);
  mytree->Branch("lep_m", &m_lep_m);
  mytree->Branch("lep_pdgid", &m_lep_pdgid);

  // Prepare branches for electrons
  m_el_pt = new std::vector<float>();
  m_el_eta = new std::vector<float>();
  m_el_phi = new std::vector<float>();
  m_el_m = new std::vector<float>();

  mytree->Branch("el_pt", &m_el_pt);
  mytree->Branch("el_eta", &m_el_eta);
  mytree->Branch("el_phi", &m_el_phi);
  mytree->Branch("el_m", &m_el_m);

  // Prepare branches for muons
  m_mu_pt = new std::vector<float>();
  m_mu_eta = new std::vector<float>();
  m_mu_phi = new std::vector<float>();
  m_mu_m = new std::vector<float>();

  mytree->Branch("mu_pt", &m_mu_pt);
  mytree->Branch("mu_eta", &m_mu_eta);
  mytree->Branch("mu_phi", &m_mu_phi);
  mytree->Branch("mu_m", &m_mu_m);

  // Prepare branches for taus
  m_tau_pt = new std::vector<float>();
  m_tau_eta = new std::vector<float>();
  m_tau_phi = new std::vector<float>();
  m_tau_m = new std::vector<float>();

  mytree->Branch("tau_pt", &m_tau_pt);
  mytree->Branch("tau_eta", &m_tau_eta);
  mytree->Branch("tau_phi", &m_tau_phi);
  mytree->Branch("tau_m", &m_tau_m);

  // Prepare branches for quraks
  m_q_pt = new std::vector<float>();
  m_q_eta = new std::vector<float>();
  m_q_phi = new std::vector<float>();
  m_q_m = new std::vector<float>();
  m_q_pdgid = new std::vector<int>();

  mytree->Branch("q_pt", &m_q_pt);
  mytree->Branch("q_eta", &m_q_eta);
  mytree->Branch("q_phi", &m_q_phi);
  mytree->Branch("q_m", &m_q_m);
  mytree->Branch("q_pdgid", &m_q_pdgid);

  // Prepare branches for jets
  m_jet_pt = new std::vector<float>();
  m_jet_eta = new std::vector<float>();
  m_jet_phi = new std::vector<float>();
  m_jet_m = new std::vector<float>();

  mytree->Branch("jet_pt", &m_jet_pt);
  mytree->Branch("jet_eta", &m_jet_eta);
  mytree->Branch("jet_phi", &m_jet_phi);
  mytree->Branch("jet_m", &m_jet_m);

  // Prepare branches for MET
  mytree->Branch("met", &m_met);

  // Book output histograms
  ANA_CHECK(book(TH1F("h_N_d0", "h_N_d0", 5, -0.5, 4.5)));
  ANA_CHECK(book(TH1F("h_N_dp", "h_N_dp", 5, -0.5, 4.5)));
  ANA_CHECK(book(TH1F("h_N_dpp", "h_N_dpp", 5, -0.5, 4.5)));
  ANA_CHECK(book(TH1F("h_N_chi", "h_N_chi", 5, -0.5, 4.5)));

  ANA_CHECK(book(TH1F("h_pdgid_d0", "h_pdgid_d0", 3, 42.5, 45.5)));
  ANA_CHECK(book(TH1F("h_pdgid_dp", "h_pdgid_dp", 78, -39.5, 39.5)));
  ANA_CHECK(book(TH1F("h_pdgid_dpp", "h_pdgid_dpp", 124, -62.5, 62.5)));
  ANA_CHECK(book(TH1F("h_pdgid_chi", "h_pdgid_chi", 3, 60.5, 63.5)));

  ANA_CHECK(book(TH1F("h_m_d0", "h_m_d0", 100, 0, 2000)));
  ANA_CHECK(book(TH1F("h_m_dp", "h_m_dp", 100, 0, 2000)));
  ANA_CHECK(book(TH1F("h_m_dpp", "h_m_dpp", 100, 0, 2000)));
  ANA_CHECK(book(TH1F("h_m_chi", "h_m_chi", 100, 0, 2000)));

  ANA_CHECK(book(TH1F("h_N_el_cascades", "h_N_el_cascades", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_N_el_other", "h_N_el_other", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_pt_el", "h_pt_el", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_el", "h_eta_el", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_el", "h_phi_el", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH1F("h_N_mu_cascades", "h_N_mu_cascades", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_N_mu_other", "h_N_mu_other", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_pt_mu", "h_pt_mu", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_mu", "h_eta_mu", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_mu", "h_phi_mu", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH1F("h_N_tau_cascades", "h_N_tau_cascades", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_N_tau_other", "h_N_tau_other", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_pt_tau", "h_pt_tau", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_tau", "h_eta_tau", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_tau", "h_phi_tau", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH2F("h_N_el_vs_N_mu", "h_N_el_vs_N_mu", 5, -0.5, 4.5, 5, -0.5, 4.5)));

  ANA_CHECK(book(TH1F("h_N_lep_cascades", "h_N_lep_cascades", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_N_lep_other", "h_N_lep_other", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_pdgid_lep", "h_pdgid_lep", 32, -16.5, 16.5)));
  ANA_CHECK(book(TH1F("h_pt_lep", "h_pt_lep", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_lep", "h_eta_lep", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_lep", "h_phi_lep", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH1F("h_N_q_cascades", "h_N_q_cascades", 8, -0.5, 7.5)));
  ANA_CHECK(book(TH1F("h_N_q_other", "h_N_q_other", 8, -0.5, 7.5)));
  ANA_CHECK(book(TH1F("h_pdgid_q", "h_pdgid_q", 14, -7.5, 7.5)));
  ANA_CHECK(book(TH1F("h_pt_q", "h_pt_q", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_q", "h_eta_q", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_q", "h_phi_q", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH1F("h_N_jet", "h_N_jet", 16, -0.5, 15.5)));
  ANA_CHECK(book(TH1F("h_pt_jet", "h_pt_jet", 50, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_jet", "h_eta_jet", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_jet", "h_phi_jet", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH1F("h_met", "h_met", 50, 0, 1000)));

  return StatusCode::SUCCESS;
}

StatusCode MCValAlgCASCADEs::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees. This is where most of your actual analysis
  // code will go.

  // Truth particles
  std::string branch = m_format == "TRUTH1" ? "TruthParticles" : "TruthBSMWithDecayParticles";
  const xAOD::EventInfo *eventInfo = nullptr;
  const xAOD::TruthParticleContainer *truth_particles = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
  ANA_CHECK(evtStore()->retrieve(truth_particles, branch));

  std::vector<const xAOD::TruthParticle *> vec_d0;
  std::vector<const xAOD::TruthParticle *> vec_dp;
  std::vector<const xAOD::TruthParticle *> vec_dpp;
  std::vector<const xAOD::TruthParticle *> vec_chi;
  std::vector<const xAOD::TruthParticle *> vec_lep;
  std::vector<const xAOD::TruthParticle *> vec_lep_other;
  std::vector<const xAOD::TruthParticle *> vec_el;
  std::vector<const xAOD::TruthParticle *> vec_el_other;
  std::vector<const xAOD::TruthParticle *> vec_mu;
  std::vector<const xAOD::TruthParticle *> vec_mu_other;
  std::vector<const xAOD::TruthParticle *> vec_tau;
  std::vector<const xAOD::TruthParticle *> vec_tau_other;
  std::vector<const xAOD::TruthParticle *> vec_q;
  std::vector<const xAOD::TruthParticle *> vec_q_other;

  for (auto tp : *truth_particles)
  {

    // Skip particles that come from hadron decays
    if (!Truth::notFromHadron(tp))
      continue;

    // Skip gluons
    if (MC::isGluon(tp->pdgId()))
      continue;

    // BSM particles
    if (std::abs(tp->pdgId()) == m_pdgIdD0)
    {
      if (tp->pdgId() == tp->child(0)->pdgId())
        continue;

      vec_d0.push_back(tp);
    }
    else if (std::abs(tp->pdgId()) == m_pdgIdDP)
    {
      if (tp->pdgId() == tp->child(0)->pdgId())
        continue;

      vec_dp.push_back(tp);
    }
    else if (std::abs(tp->pdgId()) == m_pdgIdDPP)
    {
      if (tp->pdgId() == tp->child(0)->pdgId())
        continue;

      vec_dpp.push_back(tp);
    }
    else if (std::abs(tp->pdgId()) == m_pdgIdChi)
    {
      if (tp->pdgId() == tp->child(0)->pdgId())
        continue;

      vec_chi.push_back(tp);
    }

    if (MC::isElectron(tp) && !Truth::isFromPhoton(tp))
    {
      // Skip if the particle falls out of the detector acceptance if requested by m_detector
      if (m_detector && !Truth::passDetector(tp, 4.5, 2.47, true))
        continue;

      if (Truth::isFromParticle(tp, m_pdgIdDPP))
      {
        vec_el.push_back(tp);
        vec_lep.push_back(tp);
      }
      else
      {
        vec_el_other.push_back(tp);
        vec_lep_other.push_back(tp);
      }
    }

    if (MC::isMuon(tp) && !Truth::isFromPhoton(tp))
    {
      // Skip if the particle falls out of the detector acceptance if requested by m_detector
      if (m_detector && !Truth::passDetector(tp, 3., 2.7, true))
        continue;

      if (Truth::isFromParticle(tp, m_pdgIdDPP))
      {
        vec_mu.push_back(tp);
        vec_lep.push_back(tp);
      }
      else
      {
        vec_mu_other.push_back(tp);
        vec_lep_other.push_back(tp);
      }
    }

    if (MC::isTau(tp))
    {
      if (Truth::isFromParticle(tp, m_pdgIdDPP))
      {
        vec_tau.push_back(tp);
        vec_lep.push_back(tp);
      }
      else
      {
        vec_tau_other.push_back(tp);
        vec_lep_other.push_back(tp);
      }
    }

    if (MC::isQuark(tp) && !Truth::isFromGluon(tp) && std::abs(tp->pdgId()) != 5)
    {
      // Skip if the particle falls out of the detector acceptance if requested by m_detector
      if (m_detector && !Truth::passDetector(tp, 20., 4.9, true))
        continue;

      if (Truth::isFromParticle(tp, m_pdgIdDPP))
      {
        vec_q.push_back(tp);
      }
      else
      {
        vec_q_other.push_back(tp);
      }
    }
  }

  hist("h_N_d0")->Fill(vec_d0.size());
  hist("h_N_dp")->Fill(vec_dp.size());
  hist("h_N_dpp")->Fill(vec_dpp.size());
  hist("h_N_chi")->Fill(vec_chi.size());

  for (auto d0 : vec_d0)
  {
    hist("h_m_d0")->Fill(d0->m() / Truth::GeV);
    hist("h_pdgid_d0")->Fill(44);
  }
  for (auto dp : vec_dp)
  {
    hist("h_m_dp")->Fill(dp->m() / Truth::GeV);
    hist("h_pdgid_dp")->Fill(dp->pdgId());
  }
  for (auto dpp : vec_dpp)
  {
    hist("h_m_dpp")->Fill(dpp->m() / Truth::GeV);
    hist("h_pdgid_dpp")->Fill(dpp->pdgId());
  }
  for (auto chi : vec_chi)
  {
    hist("h_m_chi")->Fill(chi->m() / Truth::GeV);
    hist("h_pdgid_chi")->Fill(chi->pdgId());
  }

  hist("h_N_el_cascades")->Fill(vec_el.size());
  hist("h_N_el_other")->Fill(vec_el_other.size());
  for (auto el : vec_el)
  {
    hist("h_pt_el")->Fill(el->pt() / Truth::GeV);
    hist("h_eta_el")->Fill(el->eta());
    hist("h_phi_el")->Fill(el->phi());
  }

  hist("h_N_mu_cascades")->Fill(vec_mu.size());
  hist("h_N_mu_other")->Fill(vec_mu_other.size());
  for (auto mu : vec_mu)
  {
    hist("h_pt_mu")->Fill(mu->pt() / Truth::GeV);
    hist("h_eta_mu")->Fill(mu->eta());
    hist("h_phi_mu")->Fill(mu->phi());
  }

  hist("h_N_tau_cascades")->Fill(vec_tau.size());
  hist("h_N_tau_other")->Fill(vec_tau_other.size());
  for (auto tau : vec_tau)
  {
    hist("h_pt_tau")->Fill(tau->pt() / Truth::GeV);
    hist("h_eta_tau")->Fill(tau->eta());
    hist("h_phi_tau")->Fill(tau->phi());
  }

  hist2d("h_N_el_vs_N_mu")->Fill(vec_el.size(), vec_mu.size());

  hist("h_N_lep_cascades")->Fill(vec_lep.size());
  hist("h_N_lep_other")->Fill(vec_lep_other.size());
  for (auto lep : vec_lep)
  {
    hist("h_pdgid_lep")->Fill(lep->pdgId());
    hist("h_pt_lep")->Fill(lep->pt() / Truth::GeV);
    hist("h_eta_lep")->Fill(lep->eta());
    hist("h_phi_lep")->Fill(lep->phi());
  }

  hist("h_N_q_cascades")->Fill(vec_q.size());
  hist("h_N_q_other")->Fill(vec_q_other.size());
  for (auto q : vec_q)
  {
    hist("h_pdgid_q")->Fill(q->pdgId());
    hist("h_pt_q")->Fill(q->pt() / Truth::GeV);
    hist("h_eta_q")->Fill(q->eta());
    hist("h_phi_q")->Fill(q->phi());
  }

  m_d0_pt->clear();
  m_d0_eta->clear();
  m_d0_phi->clear();
  m_d0_m->clear();
  m_d0_pdgid->clear();

  for (auto d0 : vec_d0)
  {
    m_d0_pt->push_back(d0->pt());
    m_d0_eta->push_back(d0->eta());
    m_d0_phi->push_back(d0->phi());
    m_d0_m->push_back(d0->m());
    m_d0_pdgid->push_back(d0->pdgId());
  }

  m_dp_pt->clear();
  m_dp_eta->clear();
  m_dp_phi->clear();
  m_dp_m->clear();
  m_dp_pdgid->clear();

  for (auto dp : vec_dp)
  {
    m_dp_pt->push_back(dp->pt());
    m_dp_eta->push_back(dp->eta());
    m_dp_phi->push_back(dp->phi());
    m_dp_m->push_back(dp->m());
    m_dp_pdgid->push_back(dp->pdgId());
  }

  m_dpp_pt->clear();
  m_dpp_eta->clear();
  m_dpp_phi->clear();
  m_dpp_m->clear();
  m_dpp_pdgid->clear();

  for (auto dpp : vec_dpp)
  {
    m_dpp_pt->push_back(dpp->pt());
    m_dpp_eta->push_back(dpp->eta());
    m_dpp_phi->push_back(dpp->phi());
    m_dpp_m->push_back(dpp->m());
    m_dpp_pdgid->push_back(dpp->pdgId());
  }

  m_chi_pt->clear();
  m_chi_eta->clear();
  m_chi_phi->clear();
  m_chi_m->clear();
  m_chi_pdgid->clear();

  for (auto chi : vec_chi)
  {
    m_chi_pt->push_back(chi->pt());
    m_chi_eta->push_back(chi->eta());
    m_chi_phi->push_back(chi->phi());
    m_chi_m->push_back(chi->m());
    m_chi_pdgid->push_back(chi->pdgId());
  }

  m_lep_pt->clear();
  m_lep_eta->clear();
  m_lep_phi->clear();
  m_lep_m->clear();
  m_lep_pdgid->clear();

  for (auto lep : vec_lep)
  {
    m_lep_pt->push_back(lep->pt());
    m_lep_eta->push_back(lep->eta());
    m_lep_phi->push_back(lep->phi());
    m_lep_m->push_back(lep->m());
    m_lep_pdgid->push_back(lep->pdgId());
  }

  m_el_pt->clear();
  m_el_eta->clear();
  m_el_phi->clear();
  m_el_m->clear();

  for (auto el : vec_el)
  {
    m_el_pt->push_back(el->pt());
    m_el_eta->push_back(el->eta());
    m_el_phi->push_back(el->phi());
    m_el_m->push_back(el->m());
  }

  m_mu_pt->clear();
  m_mu_eta->clear();
  m_mu_phi->clear();
  m_mu_m->clear();

  for (auto mu : vec_mu)
  {
    m_mu_pt->push_back(mu->pt());
    m_mu_eta->push_back(mu->eta());
    m_mu_phi->push_back(mu->phi());
    m_mu_m->push_back(mu->m());
  }

  m_tau_pt->clear();
  m_tau_eta->clear();
  m_tau_phi->clear();
  m_tau_m->clear();

  for (auto tau : vec_tau)
  {
    m_tau_pt->push_back(tau->pt());
    m_tau_eta->push_back(tau->eta());
    m_tau_phi->push_back(tau->phi());
    m_tau_m->push_back(tau->m());
  }

  m_q_pt->clear();
  m_q_eta->clear();
  m_q_phi->clear();
  m_q_m->clear();
  m_q_pdgid->clear();

  for (auto q : vec_q)
  {
    m_q_pt->push_back(q->pt());
    m_q_eta->push_back(q->eta());
    m_q_phi->push_back(q->phi());
    m_q_m->push_back(q->m());
    m_q_pdgid->push_back(q->pdgId());
  }

  // Jets
  const xAOD::JetContainer *truth_jets = nullptr;
  ANA_CHECK(evtStore()->retrieve(truth_jets, "AntiKt4TruthDressedWZJets"));

  m_jet_pt->clear();
  m_jet_eta->clear();
  m_jet_phi->clear();
  m_jet_m->clear();

  int n_jets = 0;
  for (auto jet : *truth_jets)
  {
    // Skip jets with pt < 20 GeV
    if (jet->pt() / Truth::GeV < 20.)
      continue;

    m_jet_pt->push_back(jet->pt());
    m_jet_eta->push_back(jet->eta());
    m_jet_phi->push_back(jet->phi());
    m_jet_m->push_back(jet->m());

    hist("h_pt_jet")->Fill(jet->pt() / Truth::GeV);
    hist("h_eta_jet")->Fill(jet->eta());
    hist("h_phi_jet")->Fill(jet->phi());

    n_jets++;
  }
  hist("h_N_jet")->Fill(n_jets);

  // MET
  const xAOD::MissingETContainer *truth_met = nullptr;
  ANA_CHECK(evtStore()->retrieve(truth_met, "MET_Truth"));

  for (auto met : *truth_met)
  {
    if (met->name() == "NonInt")
    {
      m_met = met->met();
    }
  }

  hist("h_met")->Fill(m_met / Truth::GeV);

  tree("analysis")->Fill();

  if (m_eventNumber > 0 && m_eventNumber % 1000 == 0)
  {
    ANA_MSG_INFO("Done " << m_eventNumber << " events.");
  }
  m_eventNumber++;
  return StatusCode::SUCCESS;
}

StatusCode MCValAlgCASCADEs::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk. This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

MCValAlgCASCADEs::~MCValAlgCASCADEs()
{

  // Delete the allocated vectors to avoid memory leaks
  delete m_d0_pt;
  delete m_d0_eta;
  delete m_d0_phi;
  delete m_d0_m;
  delete m_d0_pdgid;

  delete m_dp_pt;
  delete m_dp_eta;
  delete m_dp_phi;
  delete m_dp_m;
  delete m_dp_pdgid;

  delete m_dpp_pt;
  delete m_dpp_eta;
  delete m_dpp_phi;
  delete m_dpp_m;
  delete m_dpp_pdgid;

  delete m_chi_pt;
  delete m_chi_eta;
  delete m_chi_phi;
  delete m_chi_m;
  delete m_chi_pdgid;

  delete m_el_pt;
  delete m_el_eta;
  delete m_el_phi;
  delete m_el_m;

  delete m_mu_pt;
  delete m_mu_eta;
  delete m_mu_phi;
  delete m_mu_m;

  delete m_tau_pt;
  delete m_tau_eta;
  delete m_tau_phi;
  delete m_tau_m;

  delete m_q_pt;
  delete m_q_eta;
  delete m_q_phi;
  delete m_q_m;
  delete m_q_pdgid;

  delete m_jet_pt;
  delete m_jet_eta;
  delete m_jet_phi;
  delete m_jet_m;
}
