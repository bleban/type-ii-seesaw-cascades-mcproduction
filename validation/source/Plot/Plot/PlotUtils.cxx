#include <cmath>
#include <vector>
#include "PlotUtils.h"

namespace PlotUtils
{
    std::string extractSubstring(const std::string &input, char delimiter, int occurrence)
    {
        size_t pos = 0;
        for (int i = 0; i < occurrence; ++i)
        {
            pos = input.find(delimiter, pos);
            if (pos == std::string::npos)
            {
                return ""; // Occurrence not found
            }
            pos++; // Move past the delimiter
        }
        size_t nextPos = input.find(delimiter, pos);
        return input.substr(pos, nextPos - pos);
    }

    std::pair<int, int> findOptimalGrid(int n)
    {
        int nRows = std::floor(std::sqrt(n));
        int nCols = std::ceil(static_cast<float>(n) / nRows);

        return {nRows, nCols};
    }

    double XtoPad(double x)
    {
        double xl, yl, xu, yu;
        gPad->GetPadPar(xl, yl, xu, yu);
        double pw = xu - xl;
        double lm = gPad->GetLeftMargin();
        double rm = gPad->GetRightMargin();
        double fw = pw - pw * lm - pw * rm;
        return (x * fw + pw * lm) / pw;
    }

    double YtoPad(double y)
    {
        double xl, yl, xu, yu;
        gPad->GetPadPar(xl, yl, xu, yu);
        double ph = yu - yl;
        double tm = gPad->GetTopMargin();
        double bm = gPad->GetBottomMargin();
        double fh = ph - ph * bm - ph * tm;
        return (y * fh + bm * ph) / ph;
    }

    void setupATLASLabel(float x, float y, float fontsize)
    {
        TLatex *atlasLabel = new TLatex();
        atlasLabel->SetNDC();
        atlasLabel->SetTextSize(fontsize);
        atlasLabel->DrawLatex(x, y, "#font[72]{ATLAS} #font[42]{Truth Simulation}");
        delete atlasLabel;
    }

    void setupLegend(TLegend *legend)
    {
        int numEntries = legend->GetListOfPrimitives()->GetSize();
        double baseLegendTextSize = 0.04;
        double minLegendTextSize = 0.02;
        double legendTextSize = std::max(minLegendTextSize, baseLegendTextSize - 0.005 * (numEntries - 5));

        legend->SetTextSize(legendTextSize);
        legend->SetNColumns(numEntries > 15 ? 2 : 1);
        legend->SetBorderSize(0);
        legend->SetFillColor(0);
        legend->SetFillStyle(0);
        legend->SetTextFont(42);
    }

    void setupLogAxis(const std::string &histName, const std::vector<logVariable> &logVariables, TCanvas *canvas, TH1 *h)
    {
        for (auto var : logVariables)
            if (histName.compare(var.varName) == 0)
            {
                if (var.logx)
                {
                    canvas->SetLogx();
                }
                if (var.logy)
                {
                    h->GetYaxis()->SetRangeUser(1, 1.5 * h->GetMaximum()); // Avoid error for plotting 0 on logy
                    canvas->SetLogy();
                }
            }
    }

    void printOccupancy(TH1 *hist)
    {
        std::string histName = hist->GetTitle();
        // Print the occupancy percentages inside bins
        for (int i = 1; i <= hist->GetNbinsX(); i++)
        {
            for (int j = 1; j <= hist->GetNbinsY(); j++)
            {
                double content = hist->GetBinContent(i, j);
                double full = histName.find("_vs_") != std::string::npos ? hist->GetEntries() : hist->GetBinContent(i, 1) + hist->GetBinContent(i, 2);
                if (content > 0.)
                {
                    TLatex *l = new TLatex();
                    l->SetTextSize(0.03);
                    l->SetTextFont(42);
                    l->SetTextAlign(22);
                    l->DrawLatex(hist->GetXaxis()->GetBinCenter(i), hist->GetYaxis()->GetBinCenter(j), Form("%.1f%%", content / full * 100));
                    delete l;
                }
            }
        }
    }

    void setup2Dplots(THStack *outStack, TCanvas *canvas, TLegend *legend)
    {

        // Prepare the grid of pads for multiple mass points
        TList *histList = outStack->GetHists();
        float histCount = histList->GetSize();
        std::string histName = canvas->GetTitle();
        auto [nRows, nCols] = findOptimalGrid(histCount);
        std::vector<std::vector<TPad *>> pad(nCols, std::vector<TPad *>(nRows));
        canvasPartition(canvas, nCols, nRows, 0.001, 0.001, 0.001, 0.001);

        // Define MCTC categories
        const int nCategories = 9;
        const char *categories[nCategories] = {"isPrompt", "isStable", "isGeant", "isBSM", "uncat", "fromBSM", "fromhad", "fromTau", "isHadTau"};

        // Define strings for leptons from N
        std::string parts1, parts2, parent;

        for (int row = 0; row < nRows; row++)
        {
            for (int col = 0; col < nCols; col++)
            {
                canvas->cd(0);
                int padIdx = row * nCols + col;
                // Skip empty pads
                if (padIdx >= histCount)
                {
                    continue;
                }
                pad[col][row] = (TPad *)canvas->FindObject(TString::Format("pad_%d_%d", col, row).Data());
                pad[col][row]->Draw();
                pad[col][row]->cd();

                // Get individual mass-point
                TH1 *tmpHist = (TH1 *)histList->At(padIdx);
                tmpHist->Draw("COL");

                tmpHist->GetXaxis()->SetTitleOffset(1.);
                tmpHist->GetXaxis()->SetLabelOffset(0.01);
                tmpHist->GetYaxis()->SetTitleOffset(1.2);
                tmpHist->GetYaxis()->SetLabelOffset(0.01);

                float xPosLeg = 0.8;
                float yPosLeg = 0.7;
                if (histName == "h_ptjet1_vs_ptjet2")
                {
                    tmpHist->GetXaxis()->SetTitle("p_{T}(j_{1}) [GeV]");
                    tmpHist->GetYaxis()->SetTitle("p_{T}(j_{2}) [GeV]");
                    xPosLeg = 0.3;
                    yPosLeg = 0.6;
                }
                else if (histName.find("_vs_") != std::string::npos)
                {
                    std::regex elmu("el|mu|1|2|3");
                    std::regex eltau("el|tau|1|2|3");
                    std::regex mutau("mu|tau|1|2|3");
                    std::vector<std::regex> patterns = {elmu, eltau, mutau};
                    for (unsigned int i = 0; i < patterns.size(); i++)
                    {
                        int nlep = 0;
                        std::smatch match;
                        std::regex pattern(patterns[i]);
                        std::vector<std::string> matches;
                        std::string::const_iterator searchStart(histName.cbegin());
                        while (std::regex_search(searchStart, histName.cend(), match, pattern))
                        {
                            // Only append if unique
                            auto it = std::find(matches.begin(), matches.end(), match.str());
                            if (it == matches.end())
                            {
                                matches.push_back(match.str());
                            }
                            searchStart = match.suffix().first;
                            if (match.str() == "el" || match.str() == "mu" || match.str() == "tau")
                            {
                                nlep++;
                            }
                        }

                        if (matches.size() >= 2 && nlep >= 2)
                        {
                            parent = matches.size() == 2 ? "N" : "N_{" + matches[1] + "}";
                            if (i == 0)
                            {
                                parts1 = "electrons";
                                parts2 = "muons";
                            }
                            else if (i == 1)
                            {
                                parts1 = "electrons";
                                parts2 = "taus";
                            }
                            else if (i == 2)
                            {
                                parts1 = "muons";
                                parts2 = "taus";
                            }

                            // Prepare axes
                            tmpHist->GetXaxis()->SetTitle(Form("Number of %s", parts1.c_str()));
                            tmpHist->GetYaxis()->SetTitle(Form("Number of %s", parts2.c_str()));
                            break;
                        }
                    }
                    printOccupancy(tmpHist);
                }
                else if (histName.find("_mctc_") != std::string::npos)
                {
                    for (unsigned int cat = 0; cat < nCategories; cat++)
                    {
                        tmpHist->GetXaxis()->ChangeLabel(cat + 1, 25, -1, 31, -1, -1, categories[cat]);
                    }
                    tmpHist->GetYaxis()->SetTitle("Bit value");
                    tmpHist->GetYaxis()->SetBinLabel(1, "0");
                    tmpHist->GetYaxis()->SetBinLabel(2, "1");
                    printOccupancy(tmpHist);
                }

                // Print the "legend" as text
                TLegendEntry *entry = dynamic_cast<TLegendEntry *>(legend->GetListOfPrimitives()->At(padIdx));
                TLatex *text = new TLatex();
                text->SetNDC();
                text->SetTextAlign(22);
                text->SetTextFont(42);
                text->SetTextSize(0.08);
                text->DrawLatex(XtoPad(xPosLeg), YtoPad(yPosLeg), entry->GetLabel());
                delete text;
            }
        }

        // Print plot titles at the top and add ATLAS label
        canvas->cd(0);
        if (histName.find("_vs_") != std::string::npos)
        {
            TLatex *ltitle = new TLatex();
            ltitle->SetNDC();
            ltitle->SetTextFont(42);
            ltitle->SetTextSize(0.04);
            ltitle->DrawLatex(0.4, 0.94, ("Number of " + parts1 + " and " + parts2 + " from " + parent).c_str());
            delete ltitle;
        }
        else if (histName.find("_mctc_") != std::string::npos)
        {
            TLatex *ltitle = new TLatex();
            ltitle->SetNDC();
            ltitle->SetTextFont(42);
            ltitle->SetTextSize(0.04);
            ltitle->SetTextAlign(21);
            ltitle->DrawLatex(0.5, 0.94, Variables::getVar(canvas->GetTitle()).c_str());
            delete ltitle;
        }

        float xLeftPad = pad[0][0]->GetXlowNDC() + pad[0][0]->GetLeftMargin() * pad[0][0]->GetWNDC();
        setupATLASLabel(xLeftPad, 0.94, 0.04);
        canvas->Update();
    }

    std::vector<double> logScale(uint16_t n,
                                 double start,
                                 double end,
                                 bool rounded)
    {
        double f = std::pow(end / start, 1.0 / n);

        std::vector<double> edges;
        edges.reserve(n + 1);
        for (uint16_t bin = 0; bin <= n; bin++)
        {
            if (rounded)
            {
                edges.push_back(std::round(start * std::pow(f, bin)));
            }
            else
            {
                edges.push_back(start * std::pow(f, bin));
            }
        }

        return edges;
    }

    void processOverflow(TH1 *hist,
                         bool disableOverflow)
    {
        if (!disableOverflow)
        {
            hist->SetBinContent(hist->GetNbinsX(),
                                hist->GetBinContent(hist->GetNbinsX() + 1) + hist->GetBinContent(hist->GetNbinsX()));
        }
        // clear the overflow
        hist->SetBinContent(hist->GetNbinsX() + 1, 0);
    }

    void processUnderflow(TH1 *hist,
                          bool disableOverflow)
    {
        if (!disableOverflow)
        {
            hist->SetBinContent(1, hist->GetBinContent(0) + hist->GetBinContent(1));
        }
        // clear the underflow
        hist->SetBinContent(0, 0);
    }

    void canvasPartition(TCanvas *canvas, const int nCols, const int nRows,
                         float lMargin, float bMargin,
                         float rMargin, float tMargin)
    {
        if (!canvas)
        {
            std::cerr << "canvasPartition: canvas does not exist!" << std::endl;
            return;
        }

        // Setup Pad layout:
        float vSpacing = 0.0;
        float maxTop = 0.925;
        float vStep = (maxTop - bMargin - tMargin - (nRows - 1) * vSpacing) / nRows;

        float hSpacing = 0.0;
        float maxRight = 0.985;
        float hStep = (maxRight - lMargin - rMargin - (nCols - 1) * hSpacing) / nCols;

        float hmarl = 0.125;
        float vmard = 0.125;
        float hmarr = 0.03;
        float vmaru = 0.03;
        float vposd, vposu;
        float hposl, hposr;

        for (int col = 0; col < nCols; col++)
        {
            if (col == 0)
            {
                hposl = lMargin;
                hposr = hposl + hStep;
            }
            else if (col == nCols - 1)
            {
                hposr = maxRight;
                hposl = hposr - hStep;
            }
            else
            {
                hposl = hposr + hSpacing;
                hposr = hposl + hStep;
            }

            vposu = maxTop;
            for (int row = 0; row < nRows; row++)
            {
                if (row == 0)
                {
                    vposd = vposu - vStep;
                }
                else if (row == nRows - 1)
                {
                    vposu = vposd - vSpacing;
                    vposd = vposu - vStep;
                }
                else
                {
                    vposu = vposd - vSpacing;
                    vposd = vposu - vStep;
                }

                canvas->cd(0);

                auto name = TString::Format("pad_%d_%d", col, row);
                auto pad = (TPad *)canvas->FindObject(name.Data());
                if (pad)
                {
                    delete pad;
                }
                pad = new TPad(name.Data(), "", hposl, vposd, hposr, vposu);
                pad->SetLeftMargin(hmarl);
                pad->SetRightMargin(hmarr);
                pad->SetBottomMargin(vmard);
                pad->SetTopMargin(vmaru);
                pad->Draw();
            }
        }
    }
}